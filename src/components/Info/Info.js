import React from 'react';

const Info = () => {
    const styleObj = {
        color: 'green',
        fontSize: 30,
        padding: 20,
        border: '2px solid red'
    };

    return (
        <div>
            <p style={styleObj}>
                Мне 19 лет, я учусь в Институте на втором курсе и работаю Веб-верстальщиком в студии.
            </p>
        </div>
    )
};

export default Info;

